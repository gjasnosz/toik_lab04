package com.company;

class AtBashCipher implements Cipher {

    public String encode(final String message){
        char[] line= message.toCharArray();
        for(int i = 0; i<line.length; i++) {
            if (line[i] >= 'A' && line[i] <= 'Z') {
                line[i] = (char) ('Z' - (line[i] - 'A'));
            } else if (line[i] >= 'a' && line[i] <= 'z') {
                line[i] = (char) ('z' - (line[i] - 'a'));
            }
        }
        return new String(line);

    }
    public String decode(final String message){
       return encode(message);
    }

}
