package com.company;

interface Cipher {
    String encode(final String message);

    String decode(final String message);
}
